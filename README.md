# Project Title

Coalition Technologies - Skill Test

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

```
composer install
```

```
npm install
```

## Running the tests

Sadly, no time to to testing :( But some Dusk would've been cool.

## Authors

* **Mauricio Gonzalez** -
