
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import { TableComponent, TableColumn } from 'vue-table-component';
Vue.component('table-component', TableComponent);
Vue.component('table-column', TableColumn);
const bus = new Vue();
const app = new Vue({
    el: '#app',
    data: {
        name: null,
        quantity: null,
        price: null,
        errors: []
    },
    mounted() {
        //console.log('mounted');
    },
    methods: {
        checkForm: function(e) {
          this.errors = [];
          if(!this.name) this.errors.push("Name required.");
          if(!this.quantity) this.errors.push("Quantity required.");
          if(!this.price) this.errors.push("Price required.");
        },
        async fetchData({ page, filter, sort }) {
            const response = await axios.get('/products', { page });

            // An object that has a `data` and an optional `pagination` property
            return response;
        },
        add(e) {
            e.preventDefault();
            this.checkForm();
            if(!this.errors.length) {
                axios.post('/products', { name: this.name, quantity: this.quantity, price: this.price })
                    .then(response => {
                        alert('Product added successfully');
                        this.$refs.table.refresh();
                    });
            }
        }
    }
});
