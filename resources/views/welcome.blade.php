@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Coalition Technologies
            </div>
            <div class="links">
                <a href="https://bitbucket.org/mgonzalezcr/coalition/src/master/">Bitbucket Repo</a>
                <a href="products/create">Products</a>
            </div>
        </div>
    </div>
@endsection
