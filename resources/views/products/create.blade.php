@extends('layouts.app')

@section('content')
  <h1 class="text-center">Create a Product</h1>
  <p v-if="errors.length">
    <b>Please correct the following error(s):</b>
    <ul>
      <li v-for="error in errors">@{{ error }}</li>
    </ul>
  </p>
  <form method="post" action="{{ url('products') }}">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" v-model="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Quantity in Stock:</label>
              <input type="text" class="form-control" v-model="quantity">
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Price per Item:</label>
              <input type="text" class="form-control" v-model="price">
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4 text-center">
            <button type="submit" class="btn btn-success" @click="add($event)">Add Product</button>
          </div>
        </div>
        {{csrf_field()}}
      </form>


      <table-component :data="fetchData" ref="table" sort-by="id" sort-order="asc">
         <table-column show="id" label="ID"></table-column>
         <table-column show="name" label="Name" data-type="string"></table-column>
         <table-column show="quantity" label="Quantity" :filterable="false" data-type="numeric"></table-column>
         <table-column show="price" label="Price" :filterable="false" data-type="numeric"></table-column>
         <table-column label="" :sortable="false" :filterable="false">
           <template slot-scope="row">
              <a :href="`#${row.id}`">Edit</a>
           </template>
         </table-column>
     </table-component>
@endsection
