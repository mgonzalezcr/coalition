<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = null;
        if (File::exists('db/products.json')) {
            $products = json_decode(file_get_contents('db/products.json'), true);

            return response()->json($products['products'], 200);
        } else {
            return response()->json($products, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = null;
        if (File::exists('db/products.json')) {
            $products = json_decode(file_get_contents('db/products.json'));
            if ($products) {
                $products = $products->products;
            } else {
                $products = null;
            }
        } else {
            $products = json_decode('{"products":[]}');
            $products = null;
        }

        return view('products.create', compact('products', $products));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!file_exists('db')) {
            mkdir('db', 0777, true);
        }

        if (!File::exists('db/products.json')) {
            session(['idCount' => 1]);
            $product['id'] = 1;

            file_put_contents('db/products.json', '{"products":[' . json_encode($product) . ']}');
        } else {
            $products = json_decode(file_get_contents('db/products.json'));
            $product['id'] = session('idCount') + 1;
            session(['idCount' => session('idCount') + 1]);

            $newProduct = $product;

            $products->products[] = $newProduct;
            File::put('db/products.json', json_encode($products));
        }

        return response()->json($product, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
